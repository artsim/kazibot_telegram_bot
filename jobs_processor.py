import requests
import telegram
from telegram.ext import Updater

from context_processor import get_variable

TOKEN = "473681310:AAEd46KYO_rny-tjrrzltQpAbDP7Rv1I4Ys"

bot = telegram.Bot(TOKEN)
updater = Updater(token=TOKEN)


def send_job_details(job, user_id, first_name=None, channel=None):
    title = job["title"]
    url = job["url"]

    if first_name is not None:
        message = (
            "Hi {0}, I have found an intresting opportunity for you. "
            "Here are the details".format(first_name)
        )
    else:
        message = (
            "I have found an intresting opportunity for you. " " Here are the details"
        )

    if channel == "Telegram":
        # send the message via telegram
        chat_id = get_variable(user_id, "chat_id")
        chat_id = int(chat_id)
        bot.send_message(chat_id=chat_id, text=message)
        bot.send_message(chat_id=chat_id, text=title)
        bot.send_message(chat_id=chat_id, text=url)
    else:
        print("Unknown channel, can not send message")


def handle_posted_jobs(job_ids):
    for job_id in job_ids:
        url = "http://127.0.0.1:8000/api/jobs/{}".format(job_id)
        resp = requests.get(url, auth=("artsim", "waitisim"))
        job = resp.json()

        matched_candidates = job["matched_candidates"]

        if matched_candidates:
            for candidate in matched_candidates:
                first_name = candidate["first_name"]
                user_id = candidate["user_id"]
                channel = candidate["channel"]

                send_job_details(job, first_name, user_id, channel)


def handle_recieved_jobs(jobs, user_id):
    length = len(jobs)

    if length > 1:
        message = (
            "I have found {0} intresting opportunities for you. "
            " Here are the details"
        ).format(length)
    else:
        message = (
            "I have found an intresting opportunity for you. " " Here are the details"
        )

    chat_id = get_variable(user_id, "chat_id")
    chat_id = int(chat_id)
    bot.send_message(chat_id=chat_id, text=message)

    for job in jobs:
        title = job["title"]
        url = job["url"]
        bot.send_message(chat_id=chat_id, text=title)
        bot.send_message(chat_id=chat_id, text=url)


def fetch_jobs(update):
    user_id = update.effective_user.id
    url = "http://127.0.0.1:8000/api/candidate/{0}/jobs".format(user_id)
    resp = requests.get(url, auth=("artsim", "waitisim"))
    jobs = resp.json()
    return jobs
