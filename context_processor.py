import uuid

import redis
from pottery import RedisSet
from wit import Wit

from analytics import (event_add_user, event_new_telegram_user,
                       event_sent_message)

# from word2number import w2n

WIT_TOKEN = "XAT2DNTN6GY3E764RUEIUW3ELKTDQNGW"
# POOL = redis.ConnectionPool(host="127.0.0.1", port=6379, db=0)
POOL = redis.ConnectionPool(host="127.0.0.1", port=19791, db=0)
my_server = redis.Redis(connection_pool=POOL)


def send_message(bot, update, message):
    user_id = update.effective_user.id
    event_sent_message(user_id)
    bot.send_message(chat_id=update.message.chat_id, text=message)


def get_user_data(user_id):
    user_key = ":".join(["user", str(user_id)])
    user_data = my_server.hgetall(user_key)
    return user_data


def get_variable(user_id, key):
    user_key = ":".join(["user", str(user_id)])
    user_data = my_server.hgetall(user_key)

    try:
        key = key.encode("utf-8")
        value = str(user_data[key], "utf-8")
    except KeyError:
        value = None

    return value


def confirm_saved_data(user_id, key, value):
    print("Confirming saved data")
    user_key = ":".join(["user", str(user_id)])
    user_data = my_server.hgetall(user_key)

    if isinstance(key, str):
        key = key.encode("utf-8")

    saved_value = user_data[key]
    saved_value = saved_value.decode("utf-8")
    print("Saved value is {} and value is {}".format(saved_value, value))

    if saved_value == value:
        # set_variable(user_id, key, value)
        print("Saved value OK")
    else:
        print("Did not save value")


def set_variable(user_id, key, value):
    print("Saving {0} {1}".format(key, value))
    try:
        user_key = ":".join(["user", str(user_id)])
        user_data = my_server.hgetall(user_key)
        user_data[key] = value
        my_server.hmset(user_key, user_data)

        # confirm_saved_data(user_id, key, value)
    except Exception as e:
        raise e


def set_list_variable(user_id, key, skills):
    user_key = ":".join(["user", str(user_id)])
    user_data = my_server.hgetall(user_key)

    key = key.encode("utf-8")

    try:
        value_list_key = str(user_data[key], "utf-8")
        value_list = RedisSet(redis=my_server, key=value_list_key)

        if type(skills) is list:
            for skill in skills:
                skill = skill.strip().lstrip()
                value_list.add(skill)
        else:
            value_list.add(skill)
    except Exception as e:
        print(e.message)
        pass

    print(value_list)


def get_list_variable(user_id, key):

    try:
        value_list = RedisSet(redis=my_server, key=key)
    except KeyError as e:
        raise e

    values = []

    for value in value_list:
        values.append(value)

    return values


def check_connection():
    """test connection to the redis server"""
    try:
        response = my_server.client_list()
        print(response[0]["addr"])
    except redis.ConnectionError as e:
        print(e.message)


def set_context(user_id, context):
    """Set the context in a user conversation"""
    set_variable(user_id, "context", context)


def get_context(user_id):
    """Get the context in a user conversation"""
    context = get_variable(user_id, "context")
    return context


def create_profile(user_id):
    """Set user profile to true"""
    set_variable(user_id, "user_id", user_id)
    set_variable(user_id, "profile_created", "True")


def profile_exist(user_id):
    profile_exists = get_variable(user_id, "profile_created")
    if profile_exists == "True":
        return True
    else:
        return False


def is_profile_complete(user_id):
    profile_complete = get_variable(user_id, "profile_complete")
    if profile_complete == "True":
        return True
    else:
        return False


def skip_step(user_id):
    pass


def handle_message(text, user_id):
    resp = client.message(text)
    entities = resp["entities"]

    if "greetings" in entities:
        entity = "greetings"
    else:
        entity = ""

    action = None
    context = get_context(user_id)

    def _check_nay_entity(entities):
        try:
            entity = entities["nay"]
            print(entity)
            return True
        except KeyError:
            return False

    try:
        if entity == "greetings":
            # intent = "greetings"
            profile_complete = is_profile_complete(user_id)
            given_name = get_variable(user_id, "given_name")
            if profile_complete:
                message = "Hi {0}. How can I help you today?".format(given_name)
                action = "ask_main_menu"
            else:
                if given_name:
                    message = (
                        "Hi {0}. Would you like to resume creating your "
                        "profile? I need a complete profile for me to be "
                        "able to find and match great opportunities for "
                        "you"
                    ).format(given_name)
                else:
                    message = (
                        "Hi. Would you like to resume creating your "
                        "profile? I need a complete profile for me to be "
                        "able to find and match great opportunities for "
                        "you"
                    )
                action = "ask_resume"

                action = "ask_resume"
        elif context == "given_name":
            intent = "given_name"

            try:
                value = entities["given_name"][0]["value"]
            except KeyError:
                value = text

            set_variable(user_id, intent, value)
            create_profile(user_id)
            set_variable(user_id, "profile_step", 1)
            message = (
                "Hi {0}, Nice to meet you.\n\n" "What is your last name?"
            ).format(value)
            set_context(user_id, "last_name")
        elif context == "last_name":
            intent = "last_name"

            try:
                value = entities["last_name"][0]["value"]
            except KeyError:
                value = text

            set_variable(user_id, intent, value)
            set_variable(user_id, "profile_step", 2)
            message = "Which email address do you want me to reach you with?"
            set_context(user_id, "email")
        elif context == "email":
            intent = "email"

            try:
                value = entities["email"][0]["value"]
                set_variable(user_id, intent, value)
                set_variable(user_id, "profile_step", 3)
                message = "What is your area of expertise?"
                action = "ask_area_interest"
                set_context(user_id, "role")
            except KeyError:
                value = text
                message = (
                    f"This {value} does not look like a proper email address. "
                    "Could you please enter a proper email address"
                )
                action = "validate_email"

        elif context == "role":
            intent = "role"

            try:
                value = entities["role"][0]["value"]
            except KeyError:
                value = text

            set_variable(user_id, intent, value)
            set_variable(user_id, "profile_step", 4)
            message = "Do you have any experience in this role?" "If yes please enter the number of years, if not just type None"
            set_context(user_id, "experience")
        elif context == "experience":
            intent = "experience"

            nay = _check_nay_entity(entities)

            if not nay:
                try:
                    value = entities["number"][0]["value"]
                    # value = w2n.word_to_num(value)
                except KeyError:
                    # value = w2n.word_to_num(text)
                    value = text
            else:
                value = 0

            set_variable(user_id, "experience", value)
            set_variable(user_id, "profile_step", 5)
            message = "Which professional skills do you have related to the role? Please input as a comma separeted list."
            set_context(user_id, "skills")
        elif context == "skills":
            intent = "skills"
            nay = _check_nay_entity(entities)

            if not nay:
                try:
                    skills_list = entities["skill"]
                    skills = []
                    for skill in skills_list:
                        value = skill["value"]
                        skills.append(value)
                except KeyError:
                    if "," in text:
                        skills = text.split(",")
                    else:
                        skills = text

                set_list_variable(user_id, intent, skills)
                set_variable(user_id, "profile_step", 6)
                set_context(user_id, "frequency")
                action = "ask_frequency"
                message = "How frequently do you want to be notified on matching jobs?"
            else:
                set_variable(user_id, "profile_step", 6)
                set_context(user_id, "prompt_skills")
                message = (
                    "No skills added. I will not be able to provide you with exact matches "
                    "without you adding skills to your profile"
                )
        elif context == "frequency":
            intent = "frequency"
            set_variable(user_id, intent, value)
            set_variable(user_id, "profile_complete", "True")
            action = "submit_user_profile"
        else:
            # TODO: context not set check last profile step and carry on
            # TODO: check intent and match with context

            # if context is none
            if not context:
                message = (
                    "Oh, my mistake, it looks like we weren't talking about"
                    "anything before. Would you like to check for jobs,"
                    "update your profile, or say goodbye?"
                )
                action = "ask_main_menu"
            else:
                message = "Oh, my bad. I did not quite get that"
    except KeyError:
        pass

    return message, action


def handle_start(bot, update):
    # user = update.message.from_user
    user_id = update.effective_user.id

    event_add_user(user_id)
    event_new_telegram_user(user_id)

    user_has_profile = profile_exist(user_id)
    action = None

    if user_has_profile:
        profile_complete = is_profile_complete(user_id)
        given_name = get_variable(user_id, "given_name")
        if profile_complete:
            message = "Hi {0}. How can I help you today?".format(given_name)
            action = "ask_main_menu"
        else:
            if given_name:
                message = (
                    "Hi {0}. Would you like to resume creating your "
                    "profile? I need a complete profile for me to be "
                    "able to find and match great opportunities for "
                    "you"
                ).format(given_name)
            else:
                message = (
                    "Hi. Would you like to resume creating your "
                    "profile? I need a complete profile for me to be "
                    "able to find and match great opportunities for "
                    "you"
                )
            action = "ask_resume"
    else:
        message = "Hi there, I am KaziBot, your personal career assistaant"
        update.message.reply_text(message)

        # typing_action(bot, update)

        message = (
            "I'm happy to work together to find the right jobs for you. "
            "Before I start, I'll need some basic info about you. "
        )

        """
        "Hi! I'm Olivia, your Paradox Careers job assistant. Thank you for your interest in joining our team.
        "Let's get started! What's your first and last name?"
        """

        send_message(bot, update, message)

        message = "Let's get started! What's your first name?"
        set_context(user_id, "given_name")
        value = str(uuid.uuid4())
        set_variable(user_id, "skills", value)

    return message, action


# Setup Wit Client
client = Wit(access_token=WIT_TOKEN)
