import time

import requests
from telegram import ChatAction, InlineKeyboardButton, InlineKeyboardMarkup

from analytics import event_sent_message
from context_processor import (get_list_variable, get_user_data, get_variable,
                               set_context, set_variable)
from jobs_processor import fetch_jobs, handle_recieved_jobs


def typing_action(bot, update):
    """ Send typing action to user"""
    try:
        bot.send_chat_action(chat_id=update.message.chat_id, action=ChatAction.TYPING)
    except Exception:
        chat_id = get_variable(update.effective_user.id, "chat_id")
        bot.send_chat_action(chat_id=chat_id, action=ChatAction.TYPING)


def send_message(bot, update, message):
    """ Send message to user"""
    user_id = update.effective_user.id
    try:
        chat_id = update.message.chat.id
    except AttributeError:
        chat_id = update.effective_message.chat.id

    # log user message sent event
    event_sent_message(user_id)
    bot.send_message(chat_id=chat_id, text=message)


def _submit_user_profile(bot, update):
    user_id = get_variable(update.effective_user.id, "chat_id")
    set_variable(user_id, "profile_complete", "True")
    chat_id = get_variable(update.effective_user.id, "chat_id")
    message = "Excellent, your profile is now complete."
    send_message(bot, update, message)
    message = (
        "I will now be able to match you with exciting opportunities I come across"
    )
    send_message(bot, update, message)
    message = "Let me check current listings for matching jobs"
    send_message(bot, update, message)
    submit_user_data(chat_id)


def send_keyboard(action, message, update, bot):
    if action == "ask_resume":
        keyboard = [
            [
                InlineKeyboardButton("Resume", callback_data="callback_resume"),
                InlineKeyboardButton("Skip", callback_data="callback_skip"),
            ]
        ]

        reply_markup = InlineKeyboardMarkup(keyboard)
        update.message.reply_text(message)
        update.message.reply_text(
            "Please select one of the following:", reply_markup=reply_markup
        )
    elif action == "ask_main_menu":
        keyboard = [
            [
                InlineKeyboardButton(
                    "Check for jobs", callback_data="callback_check_jobs"
                ),
                InlineKeyboardButton(
                    "Update profile", callback_data="callback_update_profile"
                ),
                InlineKeyboardButton(
                    "Say Goodbye", callback_data="callback_end_session"
                ),
            ]
        ]

        reply_markup = InlineKeyboardMarkup(keyboard)
        update.message.reply_text(message, reply_markup=reply_markup)
    elif action == "ask_experience_level":
        keyboard = [
            [
                InlineKeyboardButton(
                    "Beginner", callback_data="callback_experience_beginner"
                ),
                InlineKeyboardButton(
                    "Intermediate", callback_data="callback_experience_intermidiate"
                ),
                InlineKeyboardButton(
                    "Advance", callback_data="callback_experience_advance"
                ),
            ]
        ]

        reply_markup = InlineKeyboardMarkup(keyboard)
        try:
            update.message.reply_text(message)
            update.message.reply_text(
                "Please select one of the following:", reply_markup=reply_markup
            )
        except Exception:
            send_message(bot, update, message)
            message = ("Please select one of the following:",)
            send_message(bot, update, message)
    elif action == "ask_area_interest":
        keyboard = [
            [
                InlineKeyboardButton(
                    "Technical", callback_data="callback_expertise_technical"
                ),
                InlineKeyboardButton(
                    "Product", callback_data="callback_expertise_product"
                ),
                InlineKeyboardButton(
                    "Business", callback_data="callback_expertise_business"
                ),
            ]
        ]

        reply_markup = InlineKeyboardMarkup(keyboard)
        try:
            update.message.reply_text(message, reply_markup=reply_markup)
        except Exception:
            chat_id = get_variable(update.effective_user.id, "chat_id")
            bot.send_message(chat_id, text=message, reply_markup=reply_markup)
    elif action == "ask_technical_role":
        roles = [
            "Software Engineer",
            "Mobile Developer",
            "Data Scientist",
            "QA Engineer",
            "DevOps Engineer",
            "Engineering Manager",
        ]
        keyboard = []
        keyboard_inlines = []

        for role in roles:
            keyboard_inlines.append(
                [
                    InlineKeyboardButton(
                        role,
                        callback_data="role_{}".format(role.split(" ", 1)[0].lower()),
                    )
                ]
            )

        keyboard = keyboard_inlines
        reply_markup = InlineKeyboardMarkup(keyboard)

        try:
            update.message.reply_text(message, reply_markup=reply_markup)
        except Exception:
            chat_id = get_variable(update.effective_user.id, "chat_id")
            bot.send_message(chat_id, text=message, reply_markup=reply_markup)
    elif action == "ask_product_role":
        roles = ["UI/UX Designer", "UX Researcher", "Product Manager", "Design Manager"]
        keyboard = []
        keyboard_inlines = []

        for role in roles:
            keyboard_inlines.append(
                [
                    InlineKeyboardButton(
                        role,
                        callback_data="role_{}".format(role.split(" ", 1)[0].lower()),
                    )
                ]
            )

        keyboard = keyboard_inlines
        reply_markup = InlineKeyboardMarkup(keyboard)

        try:
            update.message.reply_text(message, reply_markup=reply_markup)
        except Exception:
            chat_id = get_variable(update.effective_user.id, "chat_id")
            bot.send_message(chat_id, text=message, reply_markup=reply_markup)
    elif action == "ask_business_role":
        roles = [
            "Sales",
            "Customer Success",
            "Business Development",
            "Marketing",
            "Operations",
            "Accounting",
        ]
        keyboard = []
        keyboard_inlines = []

        for role in roles:
            keyboard_inlines.append(
                [
                    InlineKeyboardButton(
                        role,
                        callback_data="role_{}".format(role.split(" ", 1)[0].lower()),
                    )
                ]
            )

        keyboard = keyboard_inlines
        reply_markup = InlineKeyboardMarkup(keyboard)

        try:
            update.message.reply_text(message, reply_markup=reply_markup)
        except Exception:
            chat_id = get_variable(update.effective_user.id, "chat_id")
            bot.send_message(chat_id, text=message, reply_markup=reply_markup)
    elif action == "submit_user_profile":
        _submit_user_profile(bot, update)
    elif action == "prompt_skills":
        chat_id = get_variable(update.effective_user.id, "chat_id")
        send_message(bot, update, message)
        message = "How frequently do you want to be notified on matching jobs?"
        send_message(bot, update, message)
        user_id = get_variable(update.effective_user.id, "chat_id")
        set_context(user_id, "frequency")
    elif action == "ask_frequency":
        message = "How frequently do you want to be notified on matching jobs?"
        keyboard = [
            [
                InlineKeyboardButton("Daily", callback_data="callback_frequency_daily"),
                InlineKeyboardButton(
                    "Weekly", callback_data="callback_frequency_weekly"
                ),
            ]
        ]

        reply_markup = InlineKeyboardMarkup(keyboard)
        try:
            update.message.reply_text(message, reply_markup=reply_markup)
        except Exception:
            chat_id = get_variable(update.effective_user.id, "chat_id")
            bot.send_message(chat_id, text=message, reply_markup=reply_markup)


def resume_profile_create(bot, query, update):
    user_id = update.effective_user.id
    profile_step = get_variable(user_id, "profile_step")

    if profile_step == "1":
        message = "What is your last name?"
    elif profile_step == "2":
        message = "What job are you intersted in?"
    elif profile_step == "3":
        message = (
            "Do you have any experience in this role?"
            "If yes please enter the number of years"
        )
    elif profile_step == "4":
        message = "Which professional skills do you have related to the role?"

    bot.send_message(chat_id=query.message.chat_id, text=message)
    send_message(bot, update, message)


def resume_callback(bot, update):
    query = update.callback_query

    if query.data == "callback_resume":
        bot.edit_message_text(
            text="Selected option to resume",
            chat_id=query.message.chat_id,
            message_id=query.message.message_id,
        )
        resume_profile_create(bot, query, update)
    elif query.data == "callback_skip":
        message = "You have chosen to skip the profile completion."
        bot.edit_message_text(
            text=message,
            chat_id=query.message.chat_id,
            message_id=query.message.message_id,
        )
    elif query.data == "callback_experience_beginner":
        message = "Setting experience level to beginner"
        bot.edit_message_text(
            text=message,
            chat_id=query.message.chat_id,
            message_id=query.message.message_id,
        )
        skill_level = "Beginner"
        set_user_skill_level(bot, update, skill_level)
    elif query.data == "callback_experience_intermidiate":
        message = "Setting experience level to intermidiate"
        bot.edit_message_text(
            text=message,
            chat_id=query.message.chat_id,
            message_id=query.message.message_id,
        )
        skill_level = "Intermidiate"
        set_user_skill_level(bot, update, skill_level)
    elif query.data == "callback_experience_advance":
        message = "Setting experience level to advance"
        bot.edit_message_text(
            text=message,
            chat_id=query.message.chat_id,
            message_id=query.message.message_id,
        )
        skill_level = "Advance"
        set_user_skill_level(bot, update, skill_level)
    elif query.data == "callback_check_jobs":
        message = "Checking for matching jobs in current listings"
        bot.edit_message_text(
            text=message,
            chat_id=query.message.chat_id,
            message_id=query.message.message_id,
        )

        check_for_jobs(bot, update)
    elif query.data == "callback_update_profile":
        message = "Here is your current profile details"
        bot.edit_message_text(
            text=message,
            chat_id=query.message.chat_id,
            message_id=query.message.message_id,
        )
        update_profile(update)
    elif query.data == "callback_end_session":
        message = "Ok, bye my friend!"
        bot.edit_message_text(
            text=message,
            chat_id=query.message.chat_id,
            message_id=query.message.message_id,
        )
    elif query.data == "callback_delete_profile":
        message = (
            "Deleting your profile. "
            "This action can not be reversed "
            "Are you sure you want to continue?"
        )
        bot.edit_message_text(
            text=message,
            chat_id=query.message.chat_id,
            message_id=query.message.message_id,
        )
        delete_profile(update)
    elif query.data == "callback_expertise_technical":
        message = "What role are you interested in?"
        action = "ask_technical_role"
        send_keyboard(action, message, update, bot)
    elif query.data == "callback_expertise_product":
        message = "What role are you interested in?"
        action = "ask_product_role"
        send_keyboard(action, message, update, bot)
    elif query.data == "callback_expertise_business":
        message = "What role are you interested in?"
        action = "ask_business_role"
        send_keyboard(action, message, update, bot)
    elif query.data.split("_", 1)[0] == "role":
        roles = [
            "Software Engineer",
            "Mobile Developer",
            "Data Scientist",
            "QA Engineer",
            "DevOps Engineer",
            "Engineering Manager",
            "UI/UX Designer",
            "UX Researcher",
            "Product Manager",
            "Design Manager",
            "Sales",
            "Customer Success",
            "Business Development",
            "Marketing",
            "Operations",
            "Accounting",
        ]

        rl = query.data.split("_", 1)[1]
        role = ""

        for r in roles:
            if r.split(" ", 1)[0].lower() == rl:
                role = r

        set_user_role(bot, update, role)
    elif query.data == "callback_frequency_daily":
        user_id = update.effective_user.id
        set_variable(user_id, "frequency", "daily")
        set_variable(user_id, "profile_complete", "True")
        _submit_user_profile(bot, update)
    elif query.data == "callback_frequency_weekly":
        user_id = update.effective_user.id
        set_variable(user_id, "frequency", "weekly")
        _submit_user_profile(bot, update)


def set_user_role(bot, update, role):
    user_id = update.effective_user.id
    set_variable(user_id, "role", role)
    set_variable(user_id, "profile_step", 3)

    if role.endswith("er"):
        message = "How many years experience do you have as a {0}?".format(role)
    else:
        message = "How many years experience do you have in {0}?".format(role)

    send_message(bot, update, message)

    set_context(user_id, "experience")


def set_user_skill_level(bot, update, skill_level):
    user_id = update.effective_user.id
    chat_id = get_variable(update.effective_user.id, "chat_id")

    set_variable(user_id, "skill_level", skill_level)
    set_variable(user_id, "profile_complete", "True")

    confirmed = confirm_user_data(bot, update)

    if confirmed:
        message = "Excellent, your profile is now complete. "
        send_message(bot, update, message)

    submit_user_data(user_id)

    message = "Checking for matching jobs in current listings"
    send_message(bot, update, message)
    check_for_jobs(update)


def confirm_user_data(bot, update):
    user_id = update.effective_user.id
    chat_id = get_variable(update.effective_user.id, "chat_id")
    user_data = get_user_data(user_id)
    text = "First Name: {} \n".format(user_data["first_name"])
    text = text + "Last Name: {} \n".format(user_data["last_name"])
    text = text + "Role: {} \n".format(user_data["role"])
    text = text + "Experience Level: {} \n".format(user_data["skill_level"])

    skills_key = user_data["skills"]
    skills = get_list_variable(user_id, skills_key)
    text = text + "Skills: {} \n".format(skills)

    # Confirm with user if data is correct
    # message = 'Is the profile data correct?'
    # bot.send_message(chat_id=chat_id, text=message)
    # bot.send_message(chat_id=chat_id, text=text)
    send_message(bot, update, text)
    return True


def check_for_jobs(bot, update):
    user_id = update.effective_user.id
    chat_id = get_variable(update.effective_user.id, "chat_id")
    jobs = fetch_jobs(update)

    if jobs:
        handle_recieved_jobs(jobs, user_id)
    else:
        message = (
            "No jobs matching your profile are currently listed. "
            "I will let you know as soon as any becomes available"
        )
        bot.send_message(chat_id=chat_id, text=message)


def update_profile(update):
    pass


def delete_profile(update):
    pass


def submit_user_data(user_id):
    print("Submitting user {} data".format(user_id))
    user_data = get_user_data(user_id)
    key = "skills".encode("utf-8")
    skills_key = str(user_data[key], "utf-8")
    skills = get_list_variable(user_id, skills_key)

    skills_list = []
    for skill in skills:
        skills_list.append({"name": skill})

    print(skills_list)

    first_name = get_variable(user_id, "given_name")
    last_name = get_variable(user_id, "last_name")
    role = get_variable(user_id, "role")
    email = get_variable(user_id, "email")
    experience = get_variable(user_id, "experience")
    frequency = get_variable(user_id, "frequency")

    payload = {
        "first_name": first_name,
        "last_name": last_name,
        "user_id": user_id,
        "role": role,
        "skills": skills_list,
        "email": email,
        "experience": experience,
        "frequency": frequency,
        "channel": "Telegram",
    }

    print(payload)

    # url = "http://127.0.0.1:8000/api/candidates/"
    url = "https://jobs.kazibot.com/api/candidates/"
    r = requests.post(url, json=payload)
    print(r.content)
