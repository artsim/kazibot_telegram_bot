import logging
import threading
import uuid

import telegram
from flask import Flask, redirect, request, send_from_directory
from telegram.ext import (CallbackQueryHandler, CommandHandler, Filters,
                          MessageHandler, Updater)

from analytics import (event_add_user, event_create_profile,
                       event_new_telegram_user, event_recieved_message)
from context_processor import handle_message, handle_start, set_variable
from jobs_processor import handle_posted_jobs
from utils import resume_callback, send_keyboard, typing_action

app = Flask(__name__)

# bot = telegram.Bot(TOKEN)
# dispatcher = Dispatcher(bot, workers=1)
# dispatcher.add_handler(CommandHandler("start", start))
logging.basicConfig(
    level=logging.DEBUG, format="%(asctime)s - %(name)s - %(levelname)s - %(message)s"
)
logger = logging.getLogger(__name__)

# Development bot token
# TOKEN = "590492585:AAFzKxnbc4PR4i58z7f6Q9Yu1tqU6Wf0QL4"

# Production bot token
TOKEN = "473681310:AAEd46KYO_rny-tjrrzltQpAbDP7Rv1I4Ys"
bot = telegram.Bot(TOKEN)
updater = Updater(TOKEN, workers=32)

logger.info("Bot started")

dispatcher = updater.dispatcher

botinfo = bot.get_me()
logger.info("Bot: @%s (%s)", botinfo.username, botinfo.first_name)

logger.info("Bot ready")


@app.route("/.well-known/acme-challenge/<challenge>")
def letsencrypt_check(challenge):
    return send_from_directory(
        ".well-known/acme-challenge", challenge, mimetype="text/plain"
    )


@app.route("/", methods=["GET"])
def handle_verification():
    print("Handling Verification: ->")
    if (
        request.args.get("hub.verify_token", "")
        == "5ujWjh0mhvS5AJczksEUGAr3AfT6dhYOu3h7ULegEm8"
    ):
        print("Verification successful!")
        return request.args.get("hub.challenge", "")
    else:
        print("Verification failed!")
        print("Redirecting to kazibot.com")
        url = "http://kazibot.com"
        return redirect(url, code=302)


@app.route("/webhooks", methods=["POST"])
def webhook_handler():
    if request.method == "POST":
        # retrieve the message in JSON and then transform it to Telegram object
        update = telegram.Update.de_json(request.get_json(force=True), bot)
        dispatcher.process_update(update)

    return "ok"


@app.route("/jobshook", methods=["POST"])
def jobshook_handler():
    if request.method == "POST":
        jobs_list = request.get_json()
        job_ids = jobs_list["jobs"]
        handle_posted_jobs(job_ids)
    return "ok"


def start(bot, update):
    print("Handling start command")
    # logger.info("Starting converstation  with %s: %f", user.first_name, user.id)
    chat_id = update.message.chat.id

    # TODO: make handle start async task
    message, action = handle_start(bot, update)

    if action is not None:
        send_keyboard(action, message, update, bot)
    else:
        # send message to user
        typing_action(bot, update)
        bot.sendMessage(chat_id=chat_id, text=message)


def handle_echo(bot, update):
    chat_id = update.message.chat.id
    message = update.message.text
    bot.sendMessage(chat_id=chat_id, text=message)
    print("handle message")


def thread_message(bot, update):
    print("handle message")
    user_id = update.effective_user.id
    chat_id = update.message.chat.id
    set_variable(user_id, "chat_id", chat_id)
    recieved_message(user_id)

    # Telegram understands UTF-8, so encode text for unicode compatibility
    # text = update.message.text.encode("utf-8")
    text = update.message.text

    message, action = handle_message(text, user_id)

    if action is not None:
        send_keyboard(action, message, update, bot)
    else:
        # send message to user
        typing_action(bot, update)
        bot.sendMessage(chat_id=chat_id, text=message)


def recieved_message(user_id):
    event_recieved_message(user_id)


def echo(bot, update):
    print("Echoing recieved message")
    thread = threading.Thread(target=handle_echo, args=(bot, update))
    thread.start()
    print("Passed recieved message")
    return "ok!", 200


def process_message(bot, update):
    print("Handling recived message")
    thread = threading.Thread(target=thread_message, args=(bot, update))
    thread.start()
    print("Passed recieved message")
    return "ok!", 200


start_handler = CommandHandler("start", start)
message_handler = MessageHandler(Filters.text, process_message)
# message_handler = MessageHandler(Filters.text, echo)
dispatcher.add_handler(start_handler)
dispatcher.add_handler(message_handler)
dispatcher.add_handler(CallbackQueryHandler(resume_callback))


if __name__ == "__main__":
    # updater.start_polling()
    # updater.idle()
    # logger.info("Bot stopped")

    app.debug = False
    app.run(threaded=True)
